;;----------------------------------------------------------------------------------------------------------------------
;; Normal mode

;;----------------------------------------------------------------------------------------------------------------------
;; ProcessNormalMode
;;
;; Checks the incoming key press and dispatches the commands based on them
;;
;; Input:
;;      A = Key code

ProcessNormalMode:
                ;; #todo: use a dispatch table
                cp      VK_LEFT
                call    z,Scroll_Left
                cp      VK_DOWN
                call    z,Scroll_Down
                cp      VK_UP
                call    z,Scroll_Up
                cp      VK_RIGHT
                call    z,Scroll_Right
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Scroll_Left

Scroll_Left:
                call    bufferGetInfo
                ld      a,(ix+BufferInfo.XOffset)
                and     a
                ret     z               ; Already at 0 so can't scroll left
                dec     a
                ld      (ix+BufferInfo.XOffset),a
                ld      a,1
                ld      (Redraw),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Scroll_Right

Scroll_Right:
                call    bufferGetInfo
                ld      a,(ix+BufferInfo.XOffset)
                cp      255
                ret     nc              ; >= 255? cannot scroll right
                inc     a
                ld      (ix+BufferInfo.XOffset),a
                ld      (Redraw),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Scroll_Up

Scroll_Up:
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Scroll_Down

Scroll_Down:
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
