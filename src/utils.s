;;----------------------------------------------------------------------------------------------------------------------
;; Various utilities required throughout all the source code
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Circular buffer

BUFFER_START    equ     $0100

; Empty buffer:
;
;       +-------------------------------------------+
;                       ^^
;                       RW
;
;       R points to just before read point
;       W points at new place to write
;       R should never meet W while reading
;
; Full buffer
;       +XXXXXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXX+
;                       ^
;                       R
;                       W

BufferInsert:
                ; Input:
                ;       B = Buffer page
                ;       C = Value to insert
                ;       IX = Pointer to read/write pointers
                ; Note:
                ;       The value that IX points to must be a 16-bit value initialised to BUFFER_START
                ;
                push    hl
                push    af

                ld      a,(ix+0)
                cp      (ix+1)          ; Has PWrite reached PRead yet?
                ret     z               ; Return without error (buffer is full)

                ld      l,(ix+1)
                ld      h,b             ; HL = write address
                ld      (hl),c          ; write value in buffer
                inc     (ix+1)
                pop     af
                pop     hl
                ret

BufferRead:     ; Input:
                ;       B = Buffer page
                ;       IX = Pointer to read/write pointers
                ; Output
                ;       A = Value
                ;       B = Buffer page
                ;       ZF = 1 if nothing to read
                ; Note:
                ;       The value that IX points to must be a 16-bit value initialised to BUFFER_START
                ;
                push    hl
                ld      a,(ix+1)
                dec     a
                cp      (ix+0)            ; Buffer is empty?
                jr      z,.finish

                inc     (ix+0)            ; Advance read pointer
                ld      l,(ix+0)
                ld      h,b             ; HL = buffer pointer
                ld      a,(hl)          ; Read data
                and     a               ; Clear ZF
.finish         pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit compare

compare16:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       CF, ZF = results of comparison
        ;
                push    hl
                and     a
                sbc     hl,de
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; max

Max:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       HL = maximum value
        ;       DE = minimum value
        ;       CF = 1 if DE was maximum
        ;
                and     a
                sbc     hl,de
                jr      c,.choose_2nd   ; HL < DE?  Choose DE!
                add     hl,de           ; Restore HL
                ret
.choose_2nd     add     hl,de
                ex      de,hl
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; alignUp8K
;; align a 16-bit value up to the nearest 8K
;;
;; Input:
;;      HL = value
;;
;; Output:
;;      HL = aligned value

alignUp8K:
        push    af
        ld      a,l
        and     a               ; LSB == 0?
        jr      nz,.alignup
        ld      a,h
        and     $1f
        jr      nz,.alignup

        ; Value is already aligned
.end    pop     af
        ret

.alignup:
        ld      l,0
        ld      a,h
        and     $e0
        add     a,$20
        ld      h,a
        jr      .end

