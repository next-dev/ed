;;----------------------------------------------------------------------------------------------------------------------
;; ZX Spectrum Next Text Editor
;; Copyright (C)2019 Matt Davies, all right reserved.
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE ZXSPECTRUMNEXT
                CSPECTMAP "ed.map"

;;----------------------------------------------------------------------------------------------------------------------
;; Memory Map

;; 2000         InitKeys, DoneKeys and initial jump
;; 2100         IM table
;; 2201         Code
;;
;; 3e00         Keyboard buffer
;; 3f00         Stack

;;----------------------------------------------------------------------------------------------------------------------

                include "src/z80n.s"            ; Z80N-specific values and macros
                include "src/keyboard.s"        ; Improved keyboard routine
                include "src/utils.s"           ; Various utilities and data structures
                include "src/filesys.s"         ; File loading and saving
                include "src/memory.s"          ; Memory management
                include "src/video.s"           ; Video management
                include "src/buffer.s"          ; Buffer management
                include "src/view.s"            ; View management
                include "src/args.s"            ; Argument handling
                include "src/mode_normal.s"     ; Normal mode

;;----------------------------------------------------------------------------------------------------------------------
;; Entry point

FileName:       ds      256
OldSP:          dw      0
Redraw:         db      1

Start:
                ld      (OldSP),sp
                ld      sp,$4000

                ; Process the arguments
                ld      de,FileName
                call    getArg
                jr      c,.load

                ; New document
                call    bufferNew
                jr      .init

.load:
                ld      ix,FileName
                call    bufferLoad

.init:
                call    initKeys
                call    initConsole
                call    textMode

.l2:
                ld      a,(Redraw)
                and     a
                jr      z,.no_draw
                call    viewRender
                xor     a
                ld      (Redraw),a

.no_draw
                call    consoleUpdate
                call    inKey
                jr      z,.l2

                push    af
                call    ProcessNormalMode
                pop     af

                cp      'q'+$80         ; Ext+Q
                jr      nz,.l2

.exit:
                di
                call    bufferDone
                nextreg $56,0
                call    doneConsole
                ei
                call    doneKeys
                ld      sp,(OldSP)
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Data

Font:           incbin  "data/font.bin"
FontSize        equ     $ - Font


                display "Final address: ",$

;;----------------------------------------------------------------------------------------------------------------------
;; Stack & buffer

                org     $3d00
LineBuffer:     ds      256             ; $3d00 = current edited line buffer
                ds      2*256           ; $3e00 = keyboard circular buffer, $3f00

;;----------------------------------------------------------------------------------------------------------------------
;; Nex generation

;                ; Start Nex information (filename, start PC, start SP)
;                SAVENEX OPEN "ed.nex", Start, $8000
;
;                ; Minimum hardware 2.0.0
;                SAVENEX CORE 2, 0, 0
;
;                ; 1st parameter: Border colour
;                ; 2nd parameter: File handle (0=open, 1=close or address=write handle to this address)
;                ; 3rd parameter: Preserve next registers (0 = set to default state)
;                ; 4th parameter: 2MB ram required? (0 = no, 1 = yes)
;                SAVENEX CFG 0, 0, 0, 0
;
;                ; Generate Nex file
;                SAVENEX BANK 2

;;----------------------------------------------------------------------------------------------------------------------
;; Binary generation

                SAVEBIN "ed", $2000, $2000


