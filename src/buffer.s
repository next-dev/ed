;;----------------------------------------------------------------------------------------------------------------------
;; Buffer management
;;----------------------------------------------------------------------------------------------------------------------

BufferPage      db      0
BufferMeta:     dw      0

                STRUCT  BufferInfo

                ; Data representation
GapStart        WORD    0       ; GapStart (real or virtual)
GapEnd          WORD    0       ; GapEnd (real)
BufferSize      WORD    0       ; Size of buffer


                ; Visual information
Top             WORD    0       ; Virtual pointer off top-left of document
XOffset         BYTE    0       ; X scroll offset.

                ENDS

;;----------------------------------------------------------------------------------------------------------------------
;; bufferNew
;; Create a brand new buffer, and page in the page with the buffer info
;;
;; Output:
;;      A = arena the buffer is loaded into, or zero for error
;;      HL = address to buffer info
;;

bufferNew:
                ; Initialise the buffer memory
                push    bc
                call    arenaNew
                jr      c,.fail_arena
                ld      (BufferPage),a
                ld      bc,BufferInfo
                call    arenaAlloc              ; Allocate memory buffer info
                jr      c,.fail_nomem
                ld      a,h
                or      $c0
                ld      h,a
                ld      (BufferMeta),hl         ; HL = address of buffer info structure

                call    arenaAlign              ; Align to next page
                ld      bc,$2000
                ld      a,(BufferPage)
                call    arenaAlloc              ; And allocate it

                push    ix
                push    hl
                pop     ix
                ld      hl,0
                ld      (ix+BufferInfo.GapStart),l
                ld      (ix+BufferInfo.GapStart+1),h    ; Initialise GapStart
                ld      (ix+BufferInfo.Top),l
                ld      (ix+BufferInfo.Top+1),h         ; Initialise Top
                ld      (ix+BufferInfo.XOffset),h       ; Initialise X scroll offset

                ld      (ix+BufferInfo.GapEnd),c
                ld      (ix+BufferInfo.GapEnd+1),b      ; Initialise GapEnd
                ld      (ix+BufferInfo.BufferSize),c
                ld      (ix+BufferInfo.BufferSize+1),b  ; Initialise BufferSize
                pop     ix

                pop     bc
                ld      a,(BufferPage)
                nextreg $56,a
                ret

.fail_nomem     ld      a,(BufferPage)
                call    arenaDone
.fail_arena     xor     a
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferLoad
;; Create a new buffer and load in a file.
;;
;; Input:
;;      IX = filename
;;
;; Output:
;;      A = arena the buffer is loaded into, or zero for error


bufferLoad:
                di
                push    bc
                push    de
                push    hl
                push    ix
                call    GetSetDrive

                ; Open the file
                ld      b,FA_READ               ; Read mode
                call    fOpen                   ; Open the file
                and     a
                jr      z,.fail_open
                ld      (.BufferHandle),a

                ; Get information on the file
                ld      ix,.FileInfo
                push    ix
                pop     hl
                rst     $08
                db      F_FSTAT
                jr      c,.fail_stat
                ld      hl,(.BufferSizeH)
                ld      a,h
                or      l
                jr      nz,.fail_stat           ; >= 64K?
                
                ; Initialise buffer memory
                call    arenaNew
                jr      c,.fail_stat            ; Not enough memory
                ld      (BufferPage),a
                ld      bc,BufferInfo
                call    arenaAlloc
                jr      c,.fail_nomem
                ld      a,h
                or      $c0
                ld      h,a                     ; HL = address of buffer info structure
                ld      (BufferMeta),hl

                ; Loop and load in the text file 8K (or less) at a time
                ld      bc,(.BufferSize)
                ld      (.BufferSizeH),bc       ; Reuse for storing original size
.l1             ld      bc,(.BufferSize)
                ld      a,b
                or      c
                jr      z,.done

                ld      a,b
                and     $e0                     ; >= 8K
                jr      z,.small
                ld      bc,$2000
.small          and     a
                ld      hl,(.BufferSize)
                sbc     hl,bc
                ld      (.BufferSize),hl        ; Update work left

                ; BC = amount of data to load
                ld      a,(BufferPage)
                call    arenaAlign              ; Allocate a whole page
                ld      a,(.BufferHandle)
                ld      ix,$c000
                call    fRead
                jr      .l1

.done           ld      a,(.BufferHandle)
                rst     8
                db      F_CLOSE

                jr      .finish

.fail_nomem     ld      a,(BufferPage)
                call    arenaDone
.fail_stat      ld      a,(.BufferHandle)
                rst     $08
                db      F_CLOSE
.fail_open      xor     a
                
.finish         
                ; Update the buffer meta
                ld      a,(BufferPage)
                nextreg $56,a
                ld      ix,(BufferMeta)
                ld      hl,(.BufferSizeH)       ; HL = size of file
                ld      (ix+BufferInfo.GapStart),l
                ld      (ix+BufferInfo.GapStart+1),h
                call    alignUp8K               ; HL = size aligned with pages
                ld      (ix+BufferInfo.GapEnd),l
                ld      (ix+BufferInfo.GapEnd+1),h
                ld      (ix+BufferInfo.BufferSize),l
                ld      (ix+BufferInfo.BufferSize+1),h

                ld      hl,0
                ld      (ix+BufferInfo.Top),hl
                ld      (ix+BufferInfo.XOffset),h

                pop     ix
                pop     hl
                pop     de
                pop     bc
                ei
                ret

.FileInfo       ds      7
.BufferSize     dw      0
.BufferSizeH    ds      2
.BufferHandle   db      0

;;----------------------------------------------------------------------------------------------------------------------
;; bufferDone
;; Destroy any buffers

bufferDone:
                ld      a,(BufferPage)
                call    arenaDone
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferGetInfo
;; Point to the information structure (and make a copy into BufferData)
;;
;; Output:
;;      IX = buffer info
;;      A = buffer page

bufferGetInfo:
                push    bc
                push    de
                push    hl
                ld      a,(BufferPage)
                nextreg $56,a
                ld      hl,(BufferMeta)
                ld      de,BufferData
                ld      bc,BufferInfo
                ldir
                pop     hl
                pop     de
                pop     bc
                ld      ix,(BufferMeta)
                ret

BufferData      ds      BufferInfo      ; Copy of current buffer data

;;----------------------------------------------------------------------------------------------------------------------
;; bufferVirtToReal
;; Convert a virtual offset into a real offset.  A virtual offset ignores the existence of the buffer gap.
;;
;; Input:
;;      HL = virtual offset
;; Output:
;;      HL = real offset
;;
;; Notes: page containing buffer's meta-information is paged in

bufferVirtToReal:
                push    bc
                push    de
                push    ix

                call    bufferGetInfo
                ld      e,(ix+BufferInfo.GapStart)
                ld      d,(ix+BufferInfo.GapStart+1)    ; DE = gap start
                call    compare16                       ; offset < gap Start?
                jr      c,.done                         ; Yes, virtual == real before gap

                ld      c,(ix+BufferInfo.GapEnd)
                ld      b,(ix+BufferInfo.GapEnd+1)      ; BC = gap end
                and     a
                sbc     hl,bc                           ; HL = offset - gapEnd
                add     hl,de                           ; HL = real offset

.done           pop     ix
                pop     de
                pop     bc
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; bufferPageIn
;; Page in the part of the file that contains the real offset.
;;
;; Input:
;;      HL = real offset
;;
;; Output:
;;      DE = real address
;;
;; curGapStart is updated with real gap address of page in 8K, or 0 if it isn't in this section.

bufferPageIn:
                push    af
                push    hl

                ; Calculate the page index
                ld      a,h
                and     $f0
                swapnib
                srl     a                       ; A = page index
                ld      e,a                     ; E = page index

                ; Figure out the actual page the data is in
                ld      a,(BufferPage)
                nextreg $56,a
                ld      hl,$c000
                ld      a,e
                inc     a                       ; Skip the meta-data page and get to the data
                add     hl,a                    ; HL = index into page buffer
                ld      a,(hl)                  ; A = real page
                nextreg $56,a                   ; Page it in!
                ld      (curBufferPage),a       ; And store it

                pop     hl
                ld      e,l
                ld      a,h
                and     $1f
                or      $c0
                ld      d,a

                pop     af
                ret

curBufferPage   db      0

;;----------------------------------------------------------------------------------------------------------------------
;; bufferRestorePage
;; This will restore the page in MMU6 that was last accessed by bufferPageIn or bufferRealAddress.

bufferRestorePage:
                ld      a,(curBufferPage)
                nextreg $56,a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferRenderRow
;; Render a row of text.
;;
;; Input:
;;      HL = real offset for start of row
;;      DE = tilemap address
;;      C = X offset
;; Output:
;;      HL = real offset for start of next row
;;      DE = tilemap address of next row

bufferRenderRow:
                push    ix
                push    bc

                push    hl
                pop     ix              ; IX = real offset (RO)

                push    de
                call    bufferPageIn    ; DE = real address
                ex      de,hl           ; HL = real address
                pop     de              ; DE = tilemap address

                ld      b,c
                ld      c,80

                ;
                ; STEP 1 - Skip B characters to find the start of a row
                ;
                ; IX = RO into document
                ; HL = Address of row
                ; DE = Tilemap address
                ; B = number of chars to skip
                ;
                ; Because of the way we store data, a line never crosses the gap
                ld      a,b
                and     a
                jr      z,.step2

.step1          ld      a,h
                cp      $e0             ; Reached end of 8K page
                jr      nz,.not_end

                ; Check to see if we're going to the next page
                ; or at the end of the document.
                push    de
                push    hl

                push    ix
                pop     de              ; DE = real offset
                ld      hl,BufferData+BufferInfo.BufferSize
                ldhl
                call    compare16       ; Are they equal?
                pop     hl
                pop     de
                jr      z,.step3        ; Yes, no data

                push    de              ; Store tilemap address
                push    ix
                pop     hl              
                call    bufferPageIn    ; DE = real address
                ex      de,hl           ; HL = real address

.not_end:       ld      a,(hl)          ; Are we EOL?
                inc     ix
                inc     hl
                cp      $a
                jr      z,.step3
                djnz    .step1

                ;
                ; STEP 2 - Write out the line to the screen
                ;
.step2          ld      a,h
                cp      $e0             ; Reached end of 8K page
                jr      nz,.not_end2

                ; Check to see if we're going to the next page
                ; or at the end of the document.
                push    de
                push    hl

                push    ix
                pop     de              ; DE = real offset
                ld      hl,BufferData+BufferInfo.BufferSize
                ldhl
                call    compare16       ; Are they equal?
                pop     hl
                pop     de
                jr      z,.step3        ; Yes, no data

                push    de              ; Store tilemap address
                push    ix
                pop     hl              
                call    bufferPageIn    ; DE = real address
                ex      de,hl           ; HL = real address

.not_end2:      ld      a,(hl)          ; Are we EOL?
                inc     ix
                inc     hl
                cp      $a
                jr      z,.step3
                ld      (de),a
                inc     de
                xor     a
                ld      (de),a
                inc     de
                dec     c
                jr      nz,.step2

                ; Reached the end of the visible portion of the line
                ; Search for the start of the next line.
.find_end       ld      a,h
                cp      $e0             ; Reached end of 8K page
                jr      nz,.not_end3

                ; Check to see if we're going to the next page
                ; or at the end of the document.
                push    de
                push    hl

                push    ix
                pop     de              ; DE = real offset
                ld      hl,BufferData+BufferInfo.BufferSize
                ldhl
                call    compare16       ; Are they equal?
                pop     hl
                pop     de
                jr      z,.end          ; Yes, no data

                push    de              ; Store tilemap address
                push    ix
                pop     hl              
                call    bufferPageIn    ; DE = real address
                ex      de,hl           ; HL = real address

.not_end3:      ld      a,(hl)
                inc     ix
                inc     hl
                cp      $a              ; Found end of line?
                jr      nz,.find_end
                jr      .end

                ;
                ; STEP 3 - Write out spaces to the end of the line
                ;

.step3          xor     a
                ld      b,c
.l1             ld      (de),a
                inc     de
                ld      (de),a
                inc     de
                djnz    .l1

.end
                pop     bc
                push    ix
                pop     hl
                pop     ix
                ret