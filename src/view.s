;;----------------------------------------------------------------------------------------------------------------------
;; View management
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; viewRender
;; Render the entire view of the editor

Title           db      "Ed (v0.1)",0

viewRender:
                ; Draw footnote
                ld      bc,$1f00
                ld      de,$0150
                ld      a,2
                call    writeSpace

                ld      a,2
                ld      bc,$1f01
                ld      de,Title
                call    writeText

                ; Set cursor
                ld      bc,$0000
                call    at
                ld      d,h
                ld      e,l                             ; DE = tile address

                ; Render the buffer of text
                call    bufferGetInfo

                ld      b,31                            ; 31 lines of text
                ld      l,(ix+BufferInfo.Top)
                ld      h,(ix+BufferInfo.Top+1)
                call    bufferVirtToReal                ; HL = real pointer
                ld      c,(ix+BufferInfo.XOffset)       ; C = X offset

.l1:
                call    bufferRenderRow                 ; DE = points to next row
                djnz    .l1

                ret

