@echo off
\sdcard\bin\sjasmplus src\ed.s --zxnext=cspect --msg=war
if not errorlevel 1 (
    \sdcard\bin\hdfmonkey put \sdcard\cspect-next-2gb.img ed dot/
)
